﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WorkshopMocks.Controllers
{
    internal class UsersSingletonUtil
    {
        private static readonly Lazy<IList<UserJson>> usersSingleton = new Lazy<IList<UserJson>>(() => InitializeUsers());

        public static IList<UserJson> GetUsers()
        {
            return usersSingleton.Value;
        }

        private static IList<UserJson> InitializeUsers()
        {
            return new List<UserJson>
            {
                new UserJson
                {
                    Id = 1,
                    Name = "Mario",
                    Surname = "Rossi",
                    Age = 28,
                    WeightHistory = GenerateWeightHistory(Sex.Male)
                },

                new UserJson
                {
                    Id = 2,
                    Name = "Alessandro",
                    Surname = "Manzoni",
                    Age = 24,
                    WeightHistory = GenerateWeightHistory(Sex.Male)
                },

                new UserJson
                {
                    Id = 3,
                    Name = "Giovanni",
                    Surname = "Verga",
                    Age = 35,
                    WeightHistory = GenerateWeightHistory(Sex.Male)
                },

                new UserJson
                {
                    Id = 4,
                    Name = "Virginia",
                    Surname = "Woolf",
                    Age = 20,
                    WeightHistory = GenerateWeightHistory(Sex.Female)
                },

                new UserJson
                {
                    Id = 5,
                    Name = "Anna",
                    Surname = "Oxa",
                    Age = 40,
                    WeightHistory = GenerateWeightHistory(Sex.Female)
                },
            };
        }

        private static IList<WeightJson> GenerateWeightHistory(Sex sex)
        {
            IList<WeightJson> weightHistory = new List<WeightJson>();

            int days = 10;
            var meanWeight = (new Random().NextDouble() * 20) + (sex == Sex.Male ? 70 : 50);

            for (int i = -days; i < 0; i++)
            {
                var date = DateTime.Today.Date.AddDays(i);
                var weightDeviation = new Random().NextDouble() * 6 - 3;
                var weight = meanWeight + weightDeviation;

                weightHistory.Add(new WeightJson()
                {
                    Weight = weight,
                    Date = date
                });
            }

            return weightHistory;
        }
    }
}
