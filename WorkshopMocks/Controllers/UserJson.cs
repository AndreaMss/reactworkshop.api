﻿using System;
using System.Collections.Generic;

namespace WorkshopMocks.Controllers
{
    public class UserJson
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public int Age { get; set; }
        public IList<WeightJson> WeightHistory { get; set; }

        public void SetUser(UserJson user)
        {
            Name = user.Name;
            Surname = user.Surname;
            Age = user.Age;
        }
    }

    public enum Sex
    {
        Male,
        Female
    }
}
