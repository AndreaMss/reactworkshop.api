﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace WorkshopMocks.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        private IList<UserJson> users;

        public UsersController() {
            this.users = UsersSingletonUtil.GetUsers();
        }


        [HttpGet]
        public IList<UserJson> GetUsers() {
            return users;
        }

        [HttpGet]
        [Route("{userId}")]
        public IActionResult GetUser([FromRoute] int userId)
        {
            var matchingUser = this.users.FirstOrDefault(u => u.Id == userId);

            if (matchingUser != null)
                return this.Ok(matchingUser);

            return this.NotFound();
        }

        [HttpPut]
        [Route("{id}")]
        public IActionResult ModifyUser([FromRoute] int id, [FromBody] UserJson user) {
            var matchingUser = users.FirstOrDefault(u => u.Id == id);

            if (user == null)
                return this.NotFound();

            matchingUser.SetUser(user);

            return this.Ok();
        }

        public interface IActionResult<T>
        {
        }
    }
}
