﻿using System;

namespace WorkshopMocks.Controllers
{
    public class WeightJson
    {
        public double Weight { get; set; }
        public DateTime Date { get; set; }
    }
}